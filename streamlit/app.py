import streamlit as st
from streamlit.web.server.websocket_headers import _get_websocket_headers
import jwt
from jwt.exceptions import DecodeError
import os

if __name__ == '__main__':
    st.write("Child app")
    try:
        encoded = st.experimental_get_query_params()["jwt"][0]
        data = jwt.decode(
            encoded, os.environ["JWT_SECRET"], algorithms="HS512")
        st.write(encoded)
        st.write(data)
    except (KeyError, DecodeError) as e:
        st.write("Could not decode JWT")
        # st.write(e)
