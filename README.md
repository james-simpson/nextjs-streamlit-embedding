# To start the streamlit app

- `cd streamlit`
- `python3 -m venv venv`
- For windows: `.\venv\Scripts\activate.bat`
- For linux: `source venv/bin/activate`
- `pip install -r requirements.txt`
- `$env:JWT_SECRET="my_secret"; streamlit run .\app.py`

# To start nextjs app

- `cd nextjs`
- `npm install` OR `yarn install`
- For windows: `$env:JWT_SECRET="my_secret"; npm run dev` OR `$env:JWT_SECRET="my_secret"; yarn dev`
- For linux: `JWT_SECRET="my_secret"; npm run dev` OR `JWT_SECRET="my_secret"; yarn dev`
