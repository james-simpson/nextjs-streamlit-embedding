import Head from 'next/head'
import { Inter } from '@next/font/google'
import styles from '@/styles/Home.module.css'
import jwt from "jwt-simple";

const inter = Inter({ subsets: ['latin'] })

type homeProps = {
  data: {
    jwt: string;
  }
}


export default function Home({ data }: homeProps) {
  return (
    <>
      <Head>
        <title>Parent App</title>
        <meta name="description" content="Parent App to Embed streamlit" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={styles.main}>
        <div>
          <h1>Parent app</h1> 
        </div>
        <div>
          <iframe width={1000} height={500} src={`http://localhost:8501?jwt=${data.jwt}`}>
          </iframe>
        </div>
      </main>
    </>
  )
}

// This gets called on every request
export async function getServerSideProps() {
  // Parent app gets data about the user from its session 
  const userData = { user: {username: "example", "mids": ["000419610060006", "000419610060007"]}}
  const secret = process.env.JWT_SECRET as string;
  const data = {jwt: jwt.encode({jwt : userData}, secret, 'HS512')};
  // Pass data to the page via props
  return { props: { data } }
}